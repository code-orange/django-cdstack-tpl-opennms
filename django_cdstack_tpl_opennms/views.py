import os
from collections import OrderedDict

import xmltodict
from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_opennms/django_cdstack_tpl_opennms"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    # Copy opennms-config-workspace modules to correct location
    config_path = (
        module_prefix
        + "/templates/image/copy-to-filesystem/usr/share/opennms/opennms-config-workspace"
    )

    new_datacollections = list()
    new_events = list()

    for module in os.listdir(path=config_path):
        module_dirs = config_path + "/" + module
        for subdir in os.listdir(path=module_dirs):
            module_parts = module_dirs + "/" + subdir
            if subdir == "datacollection":
                for file in os.listdir(module_parts):
                    original_file = open(os.path.join(module_parts, file), "rb").read()
                    zip_add_file(
                        zipfile_handler,
                        "etc/opennms/datacollection/" + file,
                        original_file,
                    )
                    xml = xmltodict.parse(original_file)

                    if "datacollection-group" not in xml:
                        continue

                    new_datacollections.append(xml["datacollection-group"]["@name"])
            if subdir == "events":
                for file in os.listdir(module_parts):
                    original_file = open(os.path.join(module_parts, file), "rb").read()
                    zip_add_file(
                        zipfile_handler, "etc/opennms/events/" + file, original_file
                    )
                    new_events.append("events/" + file)
            if subdir == "graphs":
                for file in os.listdir(module_parts):
                    original_file = open(os.path.join(module_parts, file), "rb").read()
                    zip_add_file(
                        zipfile_handler,
                        "etc/opennms/snmp-graph.properties.d/" + file,
                        original_file,
                    )

    datacollection_xml = open(
        module_prefix
        + "/templates/config-fs/copy/etc/opennms/datacollection-config.xml",
        "rb",
    ).read()
    datacollection_dict = xmltodict.parse(datacollection_xml)

    for snmp_collection in datacollection_dict["datacollection-config"][
        "snmp-collection"
    ]:
        if snmp_collection["@name"] == "default":
            for key, data_collection_groups in snmp_collection.items():
                if key == "include-collection":
                    for data_collection_group in data_collection_groups:
                        try:
                            new_datacollections.remove(
                                data_collection_group["@dataCollectionGroup"]
                            )
                        except ValueError:
                            pass

    for snmp_collection in datacollection_dict["datacollection-config"][
        "snmp-collection"
    ]:
        if snmp_collection["@name"] == "default":
            for new_datacollection in new_datacollections:
                new_data = OrderedDict()
                new_data["@dataCollectionGroup"] = new_datacollection
                snmp_collection["include-collection"].append(new_data)

    zip_add_file(
        zipfile_handler,
        "etc/opennms/datacollection-config.xml",
        xmltodict.unparse(datacollection_dict, pretty=True),
    )

    events_xml = open(
        module_prefix + "/templates/config-fs/copy/etc/opennms/eventconf.xml", "rb"
    ).read()
    events_dict = xmltodict.parse(events_xml)

    for event in events_dict["events"]["event-file"]:
        try:
            new_events.remove(event)
        except ValueError:
            pass

    for new_event in new_events:
        events_dict["events"]["event-file"].append(new_event)

    zip_add_file(
        zipfile_handler,
        "etc/opennms/eventconf.xml",
        xmltodict.unparse(events_dict, pretty=True),
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
